package com.telcomsis.bioapp.main

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import com.telcomsis.bioapp.R



/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [FingerMainFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [FingerMainFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FingerMainFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_finger_main, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


    }


}// Required empty public constructor
