package com.telcomsis.bioapp.main


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.telcomsis.bioapp.R


/**
 * A simple [Fragment] subclass.
 * Use the [FaceMainFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FaceMainFragment : Fragment() {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_face_main, container, false)
    }


}// Required empty public constructor
